
# v-outer-html directive
## Usage
| npm install v-outer-html | yarn add v-outer-html |
|--|--|

Works same as v-html but replaces the element with directive instead of putting elements inside it. 
```javascript
import Vue from 'vue';
import vOuterHTML from 'v-outer-html';
Vue.directive('outer-html', vOuterHTML);

let htmlContent = `
  <h1>Title</h1>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus quia dolorem iste expedita, molestias ipsam.
  </p>
`
```
```html
<div v-outer-html="htmlContent"></div>

<!--produces this-->
<h1>Title</h1>
<p>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus quia dolorem iste expedita, molestias ipsam.
</p>

<!--instead of this-->
<div>
  <h1>Title</h1>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus quia dolorem iste expedita, molestias ipsam.
  </p>
</div>
```